let grid = [];
var myVar;
function createGrid(width, height) {//creates 8x15 grid 
    var gameHeight = height;
    var gameWidth = width;
    for (let row = 0; row < height; row += 1) {
        grid.push([]);
        for (let col = 0; col < width; col += 1) {
            grid[row].push('E');
        }
    }
    for (i = 0; i < width; i += 1) {// makes row 0 all "M"
        grid[0].splice(i, 1, "M");
    }



    for (let i = 0; i < height; i += 1) {
        grid[i].splice(0, 1, "M");
    }

    for (let i = 0; i < height; i += 1) {
        grid[i].splice(9, 1, "M");
    }
    myVar = setInterval(blockDetect, 140);
    document.getElementById("instructions").innerHTML = "";
    document.getElementById("div").innerHTML = "";
}
let blocksRowGlobal;
let blocksColGlobal;

let blocksRowMGlobal;//global "M" lovations
let blocksColMGlobal;

let filledRowsGlobal = [];

let currentBlock = "";
let score = 0;

function rotate() { //only runs when "s" is pressed
    if (currentBlock == "2x1") {
        if (blocksRowGlobal[0] == blocksRowGlobal[1]) {
            if (grid[blocksRowGlobal[0] + 1][blocksColGlobal[0]] == "E") {
                //code to change to upright
                //deletes sideways block
                grid[blocksRowGlobal[0]].splice(blocksColGlobal[0], 1, "E");
                grid[blocksRowGlobal[1]].splice(blocksColGlobal[1], 1, "E");
                //adds upright block
                grid[blocksRowGlobal[0]].splice(blocksColGlobal[0], 1, "O");
                grid[blocksRowGlobal[0] + 1].splice(blocksColGlobal[0], 1, "O");
                ModularBlockDetect();
            }
        } else {
            if (grid[blocksRowGlobal[0]][blocksColGlobal[0] + 1] == "E") {
                //code to change to sideways
                //deletes block
                grid[blocksRowGlobal[0]].splice(blocksColGlobal[0], 1, "E");
                grid[blocksRowGlobal[1]].splice(blocksColGlobal[1], 1, "E");
                //makes new sideways block
                grid[blocksRowGlobal[0]].splice(blocksColGlobal[0], 1, "O");
                grid[blocksRowGlobal[0]].splice(blocksColGlobal[0] + 1, 1, "O");
                ModularBlockDetect();
            }
        }
    }
}

function rowDelete() {// deletes rows,

    if (filledRowsGlobal.length >= 1) {
        let blankGrid = ["M", "E", "E", "E", "E", "E", "E", "E", "E", "M"];
        for (let i = filledRowsGlobal.length - 1; i >= 0; i -= 1) {
            grid.splice(filledRowsGlobal[i], 1);
        }
        addRows();

    }
    if(filledRowsGlobal.length == 2){
        addRows();
    }
}

function addRows(){//only adds one row, make it add however many are needed
    let blankGrid = ["M", "E", "E", "E", "E", "E", "E", "E", "E", "M"];

    grid.push(blankGrid);
}

function scoreChange(){
    let scoreBoard = "SCORE: ";
    scoreBoard += score;
    document.getElementById("score").innerHTML = scoreBoard;
}

function rowDetect() { //finds filled rows and puts them in an array
    let filledRows = []; //array of row numbers that have been filled
    if(filledRowsGlobal.length > 0){
        score += filledRowsGlobal.length;
    }
    for(let row = 1; row < grid.length; row += 1){
        let MCount = 0;
        for(let col = 1; col < 9; col += 1){
            if(grid[row][col] == "M"){
                MCount += 1;
            }
            if(MCount > 7){
                filledRows.push(row);
            }
        }
    }
    filledRowsGlobal = filledRows;
    rowDelete();
    scoreChange();
}

function ModularBlockDetect(){
    let rowCountUpdate = 0;
    let blocksRowUpdate = [];//array of row location of "O" 
    let blocksColUpdate = [];//array of colum location of "O"
    for (row of grid) {
        let colCountUpdate = 0;
        for (col of grid[rowCountUpdate]) {
            if (col == "O") {
                blocksRowUpdate.push(rowCountUpdate);
                blocksColUpdate.push(colCountUpdate);
            }
            colCountUpdate += 1;
        }
        rowCountUpdate += 1;
    }
    blocksColGlobal = blocksColUpdate;
    blocksRowGlobal = blocksRowUpdate;
}

function youLose(){
    clearInterval(myVar);  
    let scoreTitle = "";
    if(score == 0){
        scoreTitle = "you are a failure.";
    }
    if(score > 0 && score < 10){
        scoreTitle = "you have achieved the bare minimum.";
    }
    if(score > 9 && score < 51){
        scoreTitle = "you have lost."
    }
    if(score > 50 && score < 100){
        scoreTitle = "your score is admirable, yet you have still failed.";
    }
    if(score > 99){
        scoreTitle = "look into your mind and tell me what you see, do you see bright lights, or do you see eternal darkness.";
    }
    let loosingGraphic = '<h1>'+scoreTitle+'</H1><br><br><br><br><h1>SCORE: '+score+'</h1>' ;
    document.getElementById("wholeThing").innerHTML = loosingGraphic;

    document.getElementById("wholeThing").innerHTML = loosingGraphic;

}

function blockDetect() {//finds and returns location of all occupied blocks when run
    if(grid[13][3] == "M" || grid[13][4] == "M"){
    youLose();
        console.log("lose");
    }
    let rowCount = 0;
    let blocksRow = [];//array of row location of "O" 
    let blocksCol = [];//array of colum location of "O"
    for (row of grid) {
        let colCount = 0;
        for (col of grid[rowCount]) {
            if (col == "O") {
                blocksRow.push(rowCount);
                blocksCol.push(colCount);
            }
            colCount += 1;
        }
        rowCount += 1;
    }

    //detects if "M" block is below any "O" blocks
    let rowCount3 = 0;
    let MDetect = "false";
    for (row of blocksRow) {
        if (grid[row - 1][blocksCol[rowCount3]] == "M") {
            MDetect = "true";
        }
        rowCount3 += 1;
    }

    if (MDetect == "true") {
        let rowCount4 = 0;//this portion runs if the above loop is true, changes all "O" blocks to "M"
        for (row of blocksRow) {
            grid[row].splice(blocksCol[rowCount4], 1, "M");
            rowCount4 += 1;
        }
    } else {//if no things are touching, "O" blocks will move down one space
        let rowCount2 = 0;
        for (row of blocksRow) {
            grid[row].splice(blocksCol[rowCount2], 1, "E");//removes "O"
            grid[row - 1].splice(blocksCol[rowCount2], 1, "O");//places "O" one row below

            rowCount2 += 1;
        }

    }
    let gameBoard = '<svg width="240" height="450" id="gameBoard">'
    //

    //finds all "M" blocks 
    let rowCountM = 0;
    let blocksRowM = [];//array of row location of "O" 
    let blocksColM = [];//array of colum location of "O"
    for (row of grid) {
        let colCountM = 0;
        for (col of grid[rowCountM]) {
            if (col == "M" || col == "O") {
                blocksRowM.push(rowCountM);
                blocksColM.push(colCountM);
            }
            colCountM += 1;
        }
        rowCountM += 1;
    }
    blocksColMGlobal = blocksColM;//updates "M:"
    blocksRowMGlobal = blocksRowM;
    //updates svg
    let colCountM2 = 0;
    for (row of blocksRowM) {
        let reverseRow = 15 - row;
        gameBoard += '<g id="group4" transform="translate(' + (blocksColM[colCountM2] - 1) * 30 + ' ' + reverseRow * 30 + ')"><circle cx="15" cy="15" r="12" fill="red" /><polygon points="15,6 12,1 20,1" fill="green" /></g>';
        colCountM2 += 1;
    }

    gameBoard += '</svg>';
    gameSvg.innerHTML = gameBoard;
    //adds new random game peices after active peices have fallen

    let blockType = Math.floor(Math.random() * 4);
    if (blocksRow.length < 1) {
        if (blockType < 2 && blockType > 0) {//2X1
            grid[14].splice(3, 1, "O");
            grid[14].splice(4, 1, "O");
            currentBlock = "2x1";
        }
        if (blockType < 3 && blockType > 1) {//2X2
            grid[14].splice(3, 1, "O");
            grid[14].splice(4, 1, "O");
            grid[13].splice(3, 1, "O");
            grid[13].splice(4, 1, "O");
            currentBlock = "2x2";
        }
        if (blockType < 4 && blockType > 2) {
            grid[14].splice(4, 1, "O");
            currentBlock = "1x1";
        }

    }

    //updates blocksRow cnd blocksCol
    let rowCountUpdate = 0;
    let blocksRowUpdate = [];//array of row location of "O" 
    let blocksColUpdate = [];//array of colum location of "O"
    for (row of grid) {
        let colCountUpdate = 0;
        for (col of grid[rowCountUpdate]) {
            if (col == "O") {
                blocksRowUpdate.push(rowCountUpdate);
                blocksColUpdate.push(colCountUpdate);
            }
            colCountUpdate += 1;
        }
        rowCountUpdate += 1;
    }
    blocksColGlobal = blocksColUpdate;
    blocksRowGlobal = blocksRowUpdate;
rowDetect();
}



function moveBlock() {
    let keyPressed = event.key;
    if (keyPressed == "a") {
        let rowCountDetect = 0;
        let MLeftDetect = "false";
        for (row of blocksRowGlobal) {
            if (grid[row][blocksColGlobal[rowCountDetect] - 1] == "M") {

                MLeftDetect = "true";
            }
            rowCountDetect += 1;
        }
        if (MLeftDetect == "false") {
            let rowCount2 = 0;
            for (row of blocksRowGlobal) {
                grid[row].splice(blocksColGlobal[rowCount2], 1, "E");//removes "O"

                rowCount2 += 1;
            }
            let rowCount3 = 0;
            for (row of blocksRowGlobal) {
                grid[row].splice(blocksColGlobal[rowCount3] - 1, 1, "O");//places "O" one row below

                rowCount3 += 1;
            }
        }
    }
    if (keyPressed == "d") {
        let rowCountDetect = 0;
        let MRightDectect = "false";
        for (row of blocksRowGlobal) {
            if (grid[row][blocksColGlobal[rowCountDetect] + 1] == "M") {
                MRightDectect = "true";
            }
            rowCountDetect += 1;
        }

        if (MRightDectect == "false") {


            let rowCount = 0;

            for (row of blocksRowGlobal) {
                grid[row].splice(blocksColGlobal[rowCount], 1, "E");//removes "O"

                rowCount += 1;
            }
            let rowCount4 = 0;
            for (row of blocksRowGlobal) {
                grid[row].splice(blocksColGlobal[rowCount4] + 1, 1, "O");//places "O" one row below

                rowCount4 += 1;
            }
        }
    }
    if(keyPressed == "s"){//runs rotating script
        rotate();
    }
    let rowCountUpdate = 0;
    let blocksRowUpdate = [];//array of row location of "O" 
    let blocksColUpdate = [];//array of colum location of "O"
    for (row of grid) {
        let colCountUpdate = 0;
        for (col of grid[rowCountUpdate]) {
            if (col == "O") {
                blocksRowUpdate.push(rowCountUpdate);
                blocksColUpdate.push(colCountUpdate);
            }
            colCountUpdate += 1;
        }
        rowCountUpdate += 1;
    }
    blocksColGlobal = blocksColUpdate;
    blocksRowGlobal = blocksRowUpdate;

    let gameBoard = '<svg width="240" height="450" id="gameBoard">'
    //

    //finds all "M" blocks 
    let rowCountM = 0;
    let blocksRowM = [];//array of row location of "O" 
    let blocksColM = [];//array of colum location of "O"
    for (row of grid) {
        let colCountM = 0;
        for (col of grid[rowCountM]) {
            if (col == "M" || col == "O") {
                blocksRowM.push(rowCountM);
                blocksColM.push(colCountM);
            }
            colCountM += 1;
        }
        rowCountM += 1;
    }
    blocksColMGlobal = blocksColM;//updates "M:"
    blocksRowMGlobal = blocksRowM;
    //updates svg
    let colCountM2 = 0;
    for (row of blocksRowM) {
        let reverseRow = 15 - row;
        gameBoard += '<g id="group4" transform="translate(' + (blocksColM[colCountM2] - 1) * 30 + ' ' + reverseRow * 30 + ')"><circle cx="15" cy="15" r="12" fill="red" /><polygon points="15,6 12,1 20,1" fill="green" /></g>';
        colCountM2 += 1;
    }

    gameBoard += '</svg>';
    gameSvg.innerHTML = gameBoard;
}


