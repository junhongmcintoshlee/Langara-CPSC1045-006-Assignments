console.log("sprite file is connected");
function drawAnimal(x, y) {
return '<g id="group1" transform="translate('+x+' '+y+')"><circle cx="15" cy="15" r="12" fill="red" /><polygon points="15,6 12,1 20,1" fill="green" /></g>'
}
function drawFruit(x, y) {
return '<g id="group2" transform="translate('+x+' '+y+')"><polygon points="10,15 1,22 1,8" stroke-width="2" stroke="black" fill="yellow" /><ellipse cx="20" cy="15" rx="10" ry="7" fill="yellow" stroke-width="2" stroke="black" /></g>'
}
function drawVehicle(x, y) {
return '<g id="group3" transform="translate('+x+' '+y+')"><circle cx="5" cy="23" r="5" fill="black" /><circle cx="25" cy="23" r="5" fill="black" /><rect x="0" y="3" width="30" height="15" fill="green" /><rect x="23" y="5" width="5" height="5" fill="white" /></g>'
}
function drawPlant(x, y) {
return '<g id="group4" transform="translate('+x+' '+y+')"><rect x="12" y="15" width="6" height="15" fill="brown" /><circle cx="15" cy="12" r="10" fill="green" /></g>'
}