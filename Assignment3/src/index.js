//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.


        function changeRadii() {
        let inputR = document.querySelector("#inputR");
        Number(inputR.value);
        let input1 = inputR.value;
        let input2 = 60 - inputR.value;
        circle1.setAttribute("r", input1);
        circle2.setAttribute("r", input2);
        console.log(circle1);
        console.log(circle2);
        var x = input1;
        var y = input2;
    }
    
    function changeLength() {
        var inputL = document.querySelector("#inputL");
        var inputLnum = Number(inputL.value);
        var hair1 = (hair.points.getItem(0).y);
        var inputL1 = inputLnum * 20 + 500;
        hair.setAttribute("points", inputL1);
        
        console.log(hair1);
        console.log(inputL1);
        console.log(inputLnum);
    }
    
    function moveEyes() {
        var eyeSlider = document.querySelector("#inputEyes");
        var eyeSliderNum = Number(eyeSlider.value);
        var eyeRight = eyeSliderNum * 2 + 580;
        var eyeLeft = eyeSliderNum * 2 + 380;
        rightEye.setAttribute("cx", eyeRight);
        leftEye.setAttribute("cx", eyeLeft);


    }

    function moveMouth() {
        var mouthSlider = document.querySelector("#inputMouth");
        var mouthSliderNum = Number(mouthSlider.value);
        var mouthLeft = mouthSliderNum * 2 + 680;
        var mouthRight = mouthSliderNum * 2 + 680;
        mouthL.setAttribute("y1", mouthLeft);
        mouthR.setAttribute("y2", mouthRight);

    }

    function changeL() {
        var lengthNum = document.querySelector("#inputL");
        var lengthNumVal = Number(lengthNum.value);
        var hairLength = lengthNumVal * 40 + 200;
        hairMove.setAttribute("height", hairLength);
    }

    function changeColour() {
        var colour = document.querySelector("#hairColour");
        var colourVal = (hairColour.value);
        hairMove.setAttribute("fill", colourVal);
        hair.setAttribute("fill", colourVal);
        console.log(colourVal);


    }
    window.moveEyes = moveEyes;
    window.changeRadii = changeRadii;
    window.changeLength = changeLength;
    window.changeL = changeL;
    window.moveMouth = moveMouth;