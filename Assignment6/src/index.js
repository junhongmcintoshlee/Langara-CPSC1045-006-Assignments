//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
    console.log("Hello world");
    function changeBoxes() {
    
        let initial = document.querySelector("#initial");
        let boxNum = document.querySelector("#boxNum");
        let growth = document.querySelector("#growth");
        let boxes = '<svg width="1000" height="500">';
        let data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
        let x = 1000 / boxNum.value;
        let big = initial.value * 5;
        
        for(let sizes of data){
            
            squareString = '<rect width="'+big+'" height="'+big+'" fill="black" id="boxesId" x="'+(x * sizes)+'" y="'+100+'" />';
            boxes += squareString;
            big = (big * growth.value);
            
        }
      
        
         boxes += "</svg>";
         box.innerHTML = boxes;
        

        
    
    }
    window.changeBoxes = changeBoxes;