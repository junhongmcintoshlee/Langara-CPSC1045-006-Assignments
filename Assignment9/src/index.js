//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function findWord(){
let words = [{word:"dog", def:"A person regarded as unpleasant, contemptible, or wicked."}, {word:"cat", def:"Ninjas in fur suit with knives hidden in the paws."}, {word:"door", def:"A device to walk through walls."}, {word:"windows", def:"A PC based operating system by Microsoft"}, {word:"car", def:"2-tonne steel carriges powered by explosions, made out of dinosaurs which are used for transportation."}, "end"];
let ans = "";
let input = document.querySelector("#textInput");
let answer = document.querySelector("#ans");
for(word of words){
    if(word.word == input.value){
        ans = word.def;
        break;
    }
    if(word == "end"){
        ans = "Not found";
    }
}
answer.innerHTML = ans;
}
window.findWord = findWord;